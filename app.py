from flask import Flask,render_template
import datetime,time
app=Flask(__name__)



@app.route('/')
def sample_page():
    now=datetime.datetime.now()
    year=now.year
    month=now.month
    day=now.day
    
    # time=now.strftime("%H:%M:%S")
    curr_time = time.localtime()
    curr_clock = time.strftime("%H:%M:%S", curr_time)

    
    monthlist=['January','February','March','April','May','June','July','August','September','October','November','December']

    return render_template('index.html',year=year,month=monthlist[month-1],day=day,time=curr_clock)




if __name__ == "__main__":
    app.run(debug=True)